package pl.ending.project.role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleDAO {

    private RoleRepository roleRepository;

    @Autowired
    public RoleDAO(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }


    public Optional<Role> finUserByRoleName(String roleName) {
        return roleRepository.findRoleByRoleName(roleName);
    }

    public Role addNewRole(String roleName) {
        return roleRepository.save(new Role(roleName));
    }


}
