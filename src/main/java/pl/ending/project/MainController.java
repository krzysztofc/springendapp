package pl.ending.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.ending.project.event.EventService;
import pl.ending.project.user.User;
import pl.ending.project.user.UserContextHolder;
import pl.ending.project.user.UserDAO;

@Controller
public class MainController {


    private UserContextHolder userContextHolder;
    private UserDAO userDAO;
    private EventService eventService;


    @Autowired
    public MainController(UserContextHolder userContextHolder, UserDAO userDAO, EventService eventService) {
        this.userContextHolder = userContextHolder;
        this.userDAO = userDAO;
        this.eventService = eventService;
    }

    @GetMapping("/")
    public String showHomePage(Model model) {
        User user = userDAO.findUserByEmail(userContextHolder.getUserLoggedIn());
        model.addAttribute("user", user);
        model.addAttribute("eventsList", eventService.findEventsAfterNow());
        return "index";
    }
}
