package pl.ending.project.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class EventDAO {

    private EventRepository eventRepository;

    @Autowired
    public EventDAO(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }


    public Event saveNewEvent(Event event) {
        return eventRepository.save(event);
    }

    public List<Event> findEventsAfterNow() {
        Date date = new Date();
        return eventRepository.findAllByFromDateAfterOrderByFromDate(date);
    }

    //all
    public List<Event> findEventsWithTileLikeSearchTile(String searchTitle) {
        return eventRepository.findAllByTitleContains(searchTitle);
    }

    //future
    public List<Event> findEventsWithTileLikeSearchTileAndAfterSearchTime(String searchTitle) {
        Date date = new Date();
        return eventRepository.findAllByTitleContainsAndToDateAfterOrderByFromDate(searchTitle, date);
    }

    //present
    public List<Event> findEventsWithTileLikeSearchTileAndDateIsPresent(String searchTitle) {
        return eventRepository.findAllByTitleAndFromDateBeforeAndToDateAfter(searchTitle);
    }

    //past
    public List<Event> findEventsWithTitleLikeSearchTitleAndToDateIsPast(String searchTitle) {
        Date date = new Date();
        return eventRepository.findAllByTitleContainsAndToDateAfter(searchTitle, date);
    }

    public Event findEventWithId(String id) {
        Integer integerId = Integer.valueOf(id);
        return eventRepository.findAllById(integerId);
    }
}
