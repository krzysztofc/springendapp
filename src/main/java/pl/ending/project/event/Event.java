package pl.ending.project.event;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.ending.project.comment.Comment;
import pl.ending.project.infrastructure.BaseEntitty;
import pl.ending.project.user.User;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@Entity
public class Event extends BaseEntitty {

    private String title;
    private Date fromDate;
    private Date toDate;
    private String description;
    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "event")
    private List<Comment> comments;

}
