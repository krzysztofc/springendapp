package pl.ending.project.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.ending.project.comment.CommentService;
import pl.ending.project.event.registration.EventRegistrationRepository;
import pl.ending.project.user.UserContextHolder;
import pl.ending.project.user.UserDAO;

import javax.validation.Valid;

@Controller
public class EventController {

    private EventService eventService;
    private CommentService commentService;
    private UserContextHolder userContextHolder;
    private UserDAO userDAO;
    private EventRegistrationRepository eventRegistrationRepository;

    @Autowired
    public EventController(EventService eventService, CommentService commentService, UserContextHolder userContextHolder, UserDAO userDAO, EventRegistrationRepository eventRegistrationRepository) {
        this.eventService = eventService;
        this.commentService = commentService;
        this.userContextHolder = userContextHolder;
        this.userDAO = userDAO;
        this.eventRegistrationRepository = eventRegistrationRepository;
    }


    @GetMapping("/event/add")
    public String showEventAddForm(Model model) {
        model.addAttribute("eventDTO", new EventDTO());
        return "eventAddForm";
    }

    @PostMapping("/event/add")
    public String addEvent(@ModelAttribute @Valid EventDTO eventDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "eventAddForm";
        }
        eventService.addNewEvent(eventDTO);
        return "redirect:/";
    }

    @GetMapping("/event/search")
    public String showEventSearchResult(@RequestParam(name = "searchTitle") String searchTitle,
                                        @RequestParam(name = "searchTime") String searchTime,
                                        Model model) {
        model.addAttribute("eventsList", eventService.findEventsByParams(searchTitle, searchTime));
        return "eventSearchResult";
    }

    @GetMapping("/event/show/{id}")
    public String showEventById(@PathVariable(name = "id") String id,
                                Model model) {
        model.addAttribute("event", eventService.findEventWithId(id));
        model.addAttribute("comment", commentService.findCommentByEventId(id));
        model.addAttribute("user", userDAO.findUserByEmail(userContextHolder.getUserLoggedIn()));
//        model.addAttribute("user", eventRegistrationRepository;
        return "eventDetails";
    }
}
