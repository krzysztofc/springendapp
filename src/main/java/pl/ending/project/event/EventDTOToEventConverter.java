package pl.ending.project.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ending.project.user.UserContextHolder;
import pl.ending.project.user.UserDAO;

@Service
public class EventDTOToEventConverter {

    private UserContextHolder userContextHolder;
    private UserDAO userDAO;

    @Autowired
    public EventDTOToEventConverter(UserContextHolder userContextHolder, UserDAO userDAO) {
        this.userContextHolder = userContextHolder;
        this.userDAO = userDAO;
    }

    public Event convertEventDTOToEvent(EventDTO eventDTO) {
        Event event = new Event();

        event.setTitle(eventDTO.getTitle());
        event.setFromDate(eventDTO.getFromDate());
        event.setToDate(eventDTO.getToDate());
        event.setDescription(eventDTO.getDescription());

        return event;
    }
}
