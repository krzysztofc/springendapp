package pl.ending.project.event;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {

    List<Event> findAllByFromDateAfterOrderByFromDate(Date date);

    //all
    List<Event> findAllByTitleContains(String searchTitle);

    //future
    List<Event> findAllByTitleContainsAndToDateAfterOrderByFromDate(String searchTitle, Date date);

    //present
    @Query("SELECT e FROM Event e " +
            "WHERE e.fromDate < current_date " +
            "AND " +
            "e.toDate > current_date " +
            "AND UPPER(e.title) LIKE CONCAT('%',UPPER(?1),'%')")
    List<Event> findAllByTitleAndFromDateBeforeAndToDateAfter(String searchTitle);

    //past todo fixme
    @Query("SELECT e FROM Event e " +
            "WHERE e.toDate < current_date " +
            "AND UPPER(e.title) LIKE CONCAT('%',UPPER(?1),'%')")
    List<Event> findAllByTitleContainsAndToDateAfter(String searchTitle, Date date);

    Event findAllById(Integer id);




}
