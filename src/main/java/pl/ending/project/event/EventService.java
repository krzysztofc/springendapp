package pl.ending.project.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ending.project.user.User;
import pl.ending.project.user.UserContextHolder;
import pl.ending.project.user.UserDAO;

import java.util.List;

@Service
public class EventService {

    private EventDTOToEventConverter eventDTOToEventConverter;
    private EventDAO eventDAO;
    private UserDAO userDAO;
    private UserContextHolder userContextHolder;

    @Autowired
    public EventService(EventDTOToEventConverter eventDTOToEventConverter, EventDAO eventDAO, UserDAO userDAO, UserContextHolder userContextHolder) {
        this.eventDTOToEventConverter = eventDTOToEventConverter;
        this.eventDAO = eventDAO;
        this.userDAO = userDAO;
        this.userContextHolder = userContextHolder;
    }

    public void addNewEvent(EventDTO eventDTO) {
        Event event = eventDTOToEventConverter.convertEventDTOToEvent(eventDTO);

        String userLoggedIn = userContextHolder.getUserLoggedIn();
        User user = userDAO.findUserByEmail(userLoggedIn);
        event.setUser(user);

        eventDAO.saveNewEvent(event);
    }

    public List<Event> findEventsAfterNow() {
        return eventDAO.findEventsAfterNow();
    }

    public List<Event> findEventsByParams(String searchTitle, String searchTime) {

        switch (searchTime) {
            case "all":
                 return eventDAO.findEventsWithTileLikeSearchTile(searchTitle);
            case "future":
                return eventDAO.findEventsWithTileLikeSearchTileAndAfterSearchTime(searchTitle);
            case "present":
                return eventDAO.findEventsWithTileLikeSearchTileAndDateIsPresent(searchTitle);
            case "past":
                return eventDAO.findEventsWithTitleLikeSearchTitleAndToDateIsPast(searchTitle);
        }
        return eventDAO.findEventsAfterNow();
    }

    public Event findEventWithId(String id) {
        return eventDAO.findEventWithId(id);
    }
}
