package pl.ending.project.event.api;

import lombok.Getter;
import lombok.Setter;
import pl.ending.project.comment.CommentDTO;
import pl.ending.project.user.User;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ApiEventDTO {

    private String title;
    private Date fromDate;
    private Date toDate;
    private String description;
    private User user;
    private List<CommentDTO> commentDTOS;

}
