package pl.ending.project.event.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiEventController {

    private EventListToApiEventListDTOConverter eventListToApiEventListDTOConverter;

    @Autowired
    public ApiEventController(EventListToApiEventListDTOConverter eventListToApiEventListDTOConverter) {
        this.eventListToApiEventListDTOConverter = eventListToApiEventListDTOConverter;
    }

    @GetMapping("/events")
    public ResponseEntity<List<ApiEventListDTO>> getEvents() {
        List<ApiEventListDTO> apiEventListDTOS = eventListToApiEventListDTOConverter.rewriteEventListsToApiEventListDTO();
        return ResponseEntity.ok(apiEventListDTOS);
    }

    @GetMapping("/event/{id}")
    public ResponseEntity<ApiEventDTO> getEvent(@PathVariable Integer id) {
        ApiEventDTO apiEventDTO = eventListToApiEventListDTOConverter.shouldReturnOneEventWithId(id);
        return ResponseEntity.ok(apiEventDTO);
    }


}
