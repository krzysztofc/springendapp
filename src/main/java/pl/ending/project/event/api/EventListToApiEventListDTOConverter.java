package pl.ending.project.event.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ending.project.comment.Comment;
import pl.ending.project.comment.CommentDTO;
import pl.ending.project.event.Event;
import pl.ending.project.event.EventRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventListToApiEventListDTOConverter {

    private EventRepository eventRepository;

    @Autowired
    public EventListToApiEventListDTOConverter(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<ApiEventListDTO> rewriteEventListsToApiEventListDTO() {
        return eventRepository.findAll().stream()
                .map(event -> rewriteEventToApiEventListDTO(event))
                .collect(Collectors.toList());
    }

    private ApiEventListDTO rewriteEventToApiEventListDTO(Event event) {
        ApiEventListDTO apiEventListDTO = new ApiEventListDTO();
        apiEventListDTO.setTitle(event.getTitle());
        apiEventListDTO.setStartDate(event.getFromDate());
        apiEventListDTO.setEndDate(event.getToDate());
        return apiEventListDTO;
    }

    public ApiEventDTO shouldReturnOneEventWithId(Integer id) {
        return rewriteEventToApiEventDTO(eventRepository.findAllById(id));


    }

    private ApiEventDTO rewriteEventToApiEventDTO(Event event) {
        ApiEventDTO apiEventDTO = new ApiEventDTO();
        apiEventDTO.setTitle(event.getTitle());
        apiEventDTO.setDescription(event.getDescription());
        apiEventDTO.setFromDate(event.getFromDate());
        apiEventDTO.setToDate(event.getToDate());
        apiEventDTO.setUser(event.getUser());

        List<CommentDTO> commentDTOS = event.getComments().stream().map(c -> new CommentDTO(c.getUser(), c.getEvent(), c.getComment(), c.getAddDate())).collect(Collectors.toList());
        apiEventDTO.setCommentDTOS(commentDTOS);

        return apiEventDTO;
    }
}
