package pl.ending.project.event.api;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
public class ApiEventListDTO {

    private String title;
    private Date startDate;
    private Date endDate;

}
