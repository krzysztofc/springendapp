package pl.ending.project.event.registration;

import lombok.Getter;
import lombok.Setter;
import pl.ending.project.event.Event;
import pl.ending.project.infrastructure.BaseEntitty;
import pl.ending.project.user.User;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Setter
@Getter
@Entity
public class EventRegistration extends BaseEntitty {

    private Date registrationDate;
    @ManyToOne
    private User user;
    @ManyToOne
    private Event event;

}
