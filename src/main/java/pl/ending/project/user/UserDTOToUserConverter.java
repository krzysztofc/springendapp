package pl.ending.project.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserDTOToUserConverter {

    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDTOToUserConverter(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }


    public User convertDTOToUser(UserRegistrationDTO userRegistrationDTO) {
        User user = new User();

        user.setLogin(userRegistrationDTO.getLogin());
        user.setPassword(passwordEncoder.encode(userRegistrationDTO.getPassword()));
        user.setDisplayName(userRegistrationDTO.getDisplayName());

        return user;

    }
}
