package pl.ending.project.user;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Setter
@Getter
@Service
public class UserRegistrationDTO {

    @Email(message = "Format wpisanego email jest niepoprawny.")
    private String login;
    @Size(min = 8, max = 30, message = "Hasło musi zawierać od {min} do {max} znaków.")
    private String password;
    @NotBlank
    @NotNull
    @Size(max = 50, message = "Wyświetlana nazwa nie może być dłuższa niż 50 znaków")
    private String displayName;
}
