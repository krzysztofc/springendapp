package pl.ending.project.user;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UserController {

    private UserRegistrationService userRegistrationService;

    private UserController(UserRegistrationService userRegistrationService) {
        this.userRegistrationService = userRegistrationService;
    }

    @GetMapping("/user/add")
    private String showAddNewUserForm(Model model) {
        model.addAttribute("userRegistrationDTO", new UserRegistrationDTO());
        return "registerForm";
    }

    @PostMapping("/user/add")
    private String addNewUser(@ModelAttribute @Valid UserRegistrationDTO userRegistrationDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "registerForm";
        }
        userRegistrationService.addNewUser(userRegistrationDTO);
        return "redirect:/";
    }

    @GetMapping("/user/login")
    private String showLoginForm() {
        return "loginForm";
    }
}
