package pl.ending.project.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ending.project.role.Role;
import pl.ending.project.role.RoleDAO;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserRegistrationService {

    private UserDAO userDAO;
    private UserDTOToUserConverter userDTOToUserConverter;
    private RoleDAO roleDAO;

    @Autowired
    private UserRegistrationService(UserDAO userDAO, UserDTOToUserConverter userDTOToUserConverter, RoleDAO roleDAO) {
        this.userDAO = userDAO;
        this.userDTOToUserConverter = userDTOToUserConverter;
        this.roleDAO = roleDAO;
    }

    public void addNewUser(UserRegistrationDTO userRegistrationDTO) {
        if (userExists(userRegistrationDTO.getLogin())) {
            throw new UserExistsException("Użytkownik o podanym mailu już istnieje, podaj inny mail.");
        }

        User user = userDTOToUserConverter.convertDTOToUser(userRegistrationDTO);
        final String roleName = "ROLE_USER";
        Role role = roleDAO.finUserByRoleName(roleName)
                .orElseGet(() -> roleDAO.addNewRole(roleName));
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);

        userDAO.saveNewUser(user);
    }

    private boolean userExists(String login) {
        if (userDAO.findUserByEmail(login) != null) {
            return true;
        }
        return false;
    }


}
