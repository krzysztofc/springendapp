package pl.ending.project.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.ending.project.role.Role;

import javax.persistence.*;
import java.util.Set;

@Entity
public class User {

    public User() {
    }

    @Id
    @GeneratedValue
    @JsonIgnore
    private Integer id;
    private String login;
    @JsonIgnore
    private String password;
    @JsonIgnore
    private String displayName;
    @ManyToMany
    @JoinTable(name = "user_role")
    @JsonIgnore
    private Set<Role> roles;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
