package pl.ending.project.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentDAO {

    private CommentRepository commentRepository;

    @Autowired
    public CommentDAO(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }


    public void addCommentToEvent(Comment newComment) {
        commentRepository.save(newComment);
    }

    public List<Comment> findCommentByEventId(String id) {
        Integer integerId = Integer.valueOf(id);
        return commentRepository.findAllByEventId(integerId);
    }
}
