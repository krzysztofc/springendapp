package pl.ending.project.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pl.ending.project.event.EventService;

import javax.validation.Valid;

@Controller
public class CommentController {

    private EventService eventService;
    private CommentService commentService;

    @Autowired
    public CommentController(EventService eventService, CommentService commentService) {
        this.eventService = eventService;
        this.commentService = commentService;
    }

    @GetMapping("/event/{id}/comment")
    public String showAddCommentForm(@PathVariable(name = "id") String id, Model model) {
        model.addAttribute("commentDTO", new CommentDTO());
        model.addAttribute("event", eventService.findEventWithId(id));
        return "eventAddCommentForm";
    }

    @PostMapping("/event/{id}/comment")
    public String addComentToEvent(@ModelAttribute @Valid CommentDTO commentDTO,
                                   BindingResult bindingResult,
                                   @PathVariable("id") String id) {
        if (bindingResult.hasErrors()) {
            return "eventAddCommentForm";
        }
        commentService.addCommentToEvent(id, commentDTO.getComment());
        return "redirect:/event/show/{id}";
    }
}
