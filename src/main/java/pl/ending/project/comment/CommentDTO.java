package pl.ending.project.comment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;
import pl.ending.project.event.Event;
import pl.ending.project.infrastructure.BaseEntitty;
import pl.ending.project.user.User;

import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@Service
@NoArgsConstructor
@AllArgsConstructor
public class CommentDTO extends BaseEntitty {

    private User user;
    private Event event;
    @Size(max = 500, message = "Komentarz nie może być dłuższy niż 500 znaków.")
    private String comment;
    private Date addDate;

}
