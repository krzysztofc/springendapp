package pl.ending.project.comment;

import lombok.Getter;
import lombok.Setter;
import pl.ending.project.event.Event;
import pl.ending.project.infrastructure.AuditEntity;
import pl.ending.project.user.User;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class Comment extends AuditEntity {

    @ManyToOne
    private User user;
    @ManyToOne
    private Event event;
    private String comment;
}
