package pl.ending.project.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ending.project.event.Event;
import pl.ending.project.event.EventService;
import pl.ending.project.user.User;
import pl.ending.project.user.UserContextHolder;
import pl.ending.project.user.UserDAO;

import java.util.Date;
import java.util.List;

@Service
public class CommentService {

    private EventService eventService;
    private UserContextHolder userContextHolder;
    private UserDAO userDAO;
    private CommentDAO commentDAO;

    @Autowired
    public CommentService(EventService eventService, UserContextHolder userContextHolder, UserDAO userDAO, CommentDAO commentDAO) {
        this.eventService = eventService;
        this.userContextHolder = userContextHolder;
        this.userDAO = userDAO;
        this.commentDAO = commentDAO;
    }

    public void addCommentToEvent(String id, String comment) {
        Comment newComment = new Comment();
        String userLoggedIn = userContextHolder.getUserLoggedIn();
        User user = userDAO.findUserByEmail(userLoggedIn);
        Event event = eventService.findEventWithId(id);

        newComment.setComment(comment);
        newComment.setAddDate(new Date());
        newComment.setEvent(event);
        newComment.setUser(user);

        commentDAO.addCommentToEvent(newComment);

    }

    public List<Comment> findCommentByEventId(String id) {
        return commentDAO.findCommentByEventId(id);
    }
}
