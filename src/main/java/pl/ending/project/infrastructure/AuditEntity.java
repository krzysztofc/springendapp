package pl.ending.project.infrastructure;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@MappedSuperclass
@Getter
@Setter
public abstract class AuditEntity extends BaseEntitty {
    @Temporal(TemporalType.TIMESTAMP)
    private Date addDate;
}
