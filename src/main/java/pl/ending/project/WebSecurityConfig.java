package pl.ending.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private PasswordEncoder passwordEncoder;
    private DataSource dataSource;

    @Autowired
    public WebSecurityConfig(PasswordEncoder passwordEncoder, DataSource dataSource) {
        this.passwordEncoder = passwordEncoder;
        this.dataSource = dataSource;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/event/add")
                .hasAnyRole("USER")
                .anyRequest()
                .permitAll()
                .and()
                .csrf().disable()
                .headers().frameOptions().disable()
                .and()
                .formLogin()
                .loginProcessingUrl("/user/login")
                .loginPage("/")
                .usernameParameter("loginLogin")
                .passwordParameter("loginPassword")
                .defaultSuccessUrl("/")
                .failureUrl("/user/login?error=1")
                .and().logout().logoutUrl("/user/logout");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .usersByUsernameQuery(
                        "SELECT u.login, u.password, 1 " +
                                "FROM user u " +
                                "WHERE u.login = ?")
                .authoritiesByUsernameQuery(
                        "SELECT u.login, r.role_name, 1 " +
                                "FROM user u " +
                                "JOIN user_role ur " +
                                "ON u.id=ur.user_id " +
                                "JOIN role r " +
                                "ON r.id=ur.roles_id " +
                                "WHERE u.login=?")
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder);
    }
}
